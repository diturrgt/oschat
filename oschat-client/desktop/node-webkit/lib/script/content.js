/**
 * Created by clarie on 14-7-22.
 */

(function(){
    oschat.Content=Content;
    var EXTEND = null;

    function Content(){
        if(EXTEND){
            EXTEND.apply(this,arguments);
        }
        this._content=$('#content');
        this.init();
        this._windowSize = new oschat.WindowSize();
    }

    Content.prototype={
        init:function(){

        },
        getHeight:function(){
            var height=this._windowSize._win.height;
            var headerHeight=$('#header').height();
            var userHeight=$('#user').height();
            var searchHeight=$('#search').parent().height();
            var footerHeight=$('#footer').height();
            var newheight=height-headerHeight-userHeight-searchHeight-footerHeight-25;
            this._content.height(newheight+'px');
            var navHeight=this._content.find('.nav').height();
            this._content.find('.tab-content')[0].style.height=newheight-navHeight+'px';
            $('#pills-tab1')[0].style.height='100%';
            $('#pills-tab1')[0].style.overflowY='auto';
            $('#pills-tab2')[0].style.height='100%';
            $('#pills-tab2')[0].style.overflowY='auto';
            $('#pills-tab3')[0].style.height='100%';
            $('#pills-tab3')[0].style.overflowY='auto';

        }


    }


})();
