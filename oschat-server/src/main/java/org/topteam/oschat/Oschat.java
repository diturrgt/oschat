package org.topteam.oschat;

import org.topteam.oschat.akka.DeadLetterListener;
import org.topteam.oschat.akka.SessionActor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.DeadLetter;
import akka.actor.Props;

/**
 * 系统上下文，存放一些单例对象
 * @author JiangFeng
 *
 */
public class Oschat {

	private static Oschat instance = new Oschat();

	private ActorSystem actorSystem;

	private ActorRef sessionActor;

	public static Oschat getInstance() {
		return instance;
	}

	private Oschat() {
	}

	public ActorSystem getAkkaSystem() {
		return actorSystem;
	}

	public void setAkkaSystem(ActorSystem actorSystem) {
		this.sessionActor = actorSystem.actorOf(
				Props.create(SessionActor.class), SessionActor.ACTOR_ID);
		ActorRef deadLetter = actorSystem.actorOf(Props.create(DeadLetterListener.class), "dead");
		actorSystem.eventStream().subscribe(deadLetter, DeadLetter.class);
		this.actorSystem = actorSystem;
	}

	public ActorRef getSessionActor() {
		return sessionActor;
	}
}
