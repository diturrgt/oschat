package org.topteam.oschat.security;

import org.topteam.oschat.message.JsonCoder;

public class Auth {

	public static class Coder extends JsonCoder<Auth> {
	}
	
	private boolean success;

	private String token;

	public Auth() {
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
