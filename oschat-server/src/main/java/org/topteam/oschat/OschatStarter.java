package org.topteam.oschat;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.ResourceUtils;
import org.topteam.oschat.util.UrlUtil;

/**
 * 服务启动类
 * @author JiangFeng
 *
 */
public class OschatStarter {

	public static void main(String[] args) {
		try {
			File f = ResourceUtils.getFile(UrlUtil.url2Utf8(UrlUtil
					.getDirFromClassLoader(OschatStarter.class))
					+ File.separatorChar
					+ "conf"
					+ File.separatorChar
					+ "log4j.properties");
			PropertyConfigurator.configure(f.getPath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		String confBase = "file:"
				+ UrlUtil.url2Utf8(UrlUtil
						.getDirFromClassLoader(OschatStarter.class))
				+ File.separatorChar + "conf" + File.separatorChar;

		@SuppressWarnings("resource")
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				confBase + "spring-config.xml");
//		DataIniter dataIniter = applicationContext.getBean(DataIniter.class);
//		dataIniter.init();
	}
}
